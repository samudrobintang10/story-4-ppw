from django.shortcuts import render

nama_pribadi = "Bintang Samudro"

# Create your views here.

def index(request):
    response = {'name' : nama_pribadi}
    return render(request, 'home/index.html', response)

def newpage(request):
    return render(request, 'home/newpage.html')